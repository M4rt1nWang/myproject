package ca.uwaterloo.cs451.a5

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.rogach.scallop._
import scala.collection.mutable.Map

class ApplyEnsembleSpamClassifierConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, model, method)
  val input = opt[String](descr = "the input test instances", required = true)
  val output = opt[String](descr = "the output directory", required = true)
  val model = opt[String](descr = "the base directory of all the classifier models", required = true)
  val method = opt[String](descr = "the ensemble technique, either average or vote", required = true)
  verify()
}

object ApplyEnsembleSpamClassifier{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new ApplyEnsembleSpamClassifierConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Model: " + args.model())
    log.info("Method: " + args.method())

    val conf = new SparkConf().setAppName("Apply Ensemble Spam Classifier")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    val model0 = sc.textFile(args.model() + "/part-00000")
      .map(line => {
        val data = line.substring(1, line.length()-1).split(",")
        (data.head.toInt, data.last.toDouble)
      }).collectAsMap()

    val model1 = sc.textFile(args.model() + "/part-00001")
      .map(line => {
        val data = line.substring(1, line.length()-1).split(",")
        (data.head.toInt, data.last.toDouble)
      }).collectAsMap()

    val model2 = sc.textFile(args.model() + "/part-00002")
      .map(line => {
        val data = line.substring(1, line.length()-1).split(",")
        (data.head.toInt, data.last.toDouble)
      }).collectAsMap()

    val model0_map = sc.broadcast(model0)
    val model1_map = sc.broadcast(model1)
    val model2_map = sc.broadcast(model2)

    def spamminess(features: Array[Int], model: scala.collection.Map[Int, Double]) : Double = {
      var score = 0d
      features.foreach(f => if (model.contains(f)) score += model(f))
      score
    }

    val textFile = sc.textFile(args.input())
    val method = args.method()

    val classifier = textFile.map(line => {
      val tokens = line.split("\\s+")
      val docid = tokens.head
      var isSpam = tokens(1)
      val features = tokens.drop(2).map(_.toInt)
      val score0 = spamminess(features, model0_map.value)
      val score1 = spamminess(features, model1_map.value)
      val score2 = spamminess(features, model2_map.value)
      if (method == "average") {
        var score = (score0 + score1 + score2) / 3
        if (score > 0) {
          (docid, isSpam, score, "spam")
        } else {
          (docid, isSpam, score, "ham")
        }
      } else {
        var vote0 = -1
        var vote1 = -1
        var vote2 = -1
        if (score0 > 0) {
          vote0 = 1
        }
        if (score1 > 0) {
          vote1 = 1
        }
        if (score2 > 0) {
          vote2 = 1
        }
        val score = vote0 + vote1 + vote2
        if (score > 0) {
          (docid, isSpam, score, "spam")
        } else {
          (docid, isSpam, score, "ham")
        }
      }
    }).saveAsTextFile(args.output())
  }
}
