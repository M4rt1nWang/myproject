package ca.uwaterloo.cs451.a5

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.rogach.scallop._
import scala.collection.mutable.Map
import scala.math.exp

class TrainSpamClassifierConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, model)
  val input = opt[String](descr = "the input training instances", required = true)
  val model = opt[String](descr = "the output directory where the model goes", required = true)
  val shuffle = opt[Boolean](descr = "shuffle data")
  verify()
}

object TrainSpamClassifier{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new TrainSpamClassifierConf(argv)

    log.info("Input: " + args.input())
    log.info("Model: " + args.model())
    if (args.shuffle()){
      log.info("Shuffle: True")
    } else {
      log.info("Shuffle:False")
    }

    val conf = new SparkConf().setAppName("Train Spam Classifier")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.model())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    var textFile = sc.textFile(args.input(), 1)
    if (args.shuffle()) {
      textFile = textFile.map(line => (scala.util.Random.nextInt(), line))
        .sortByKey()
        .map(pair => pair._2)
    }

    val w = Map[Int, Double]()

    def spamminess(features: Array[Int]) : Double = {
      var score = 0d
      features.foreach(f => if (w.contains(f)) score += w(f))
      score
    }
    
    val delta = 0.002

    val trained = textFile
      .map(line => {
        val tokens = line.split("\\s+")
        val docid = tokens.head
        var isSpam = 0
        if (tokens(1) == "spam") {
          isSpam = 1
        }
        val features = tokens.drop(2).map(_.toInt)
        (0, (docid, isSpam, features))
      })
      .groupByKey(1)
      .flatMap(line => {
        line._2.foreach(tuple => {
          val isSpam = tuple._2   // label
          val features = tuple._3 // feature vector of the training instance
          // Update the weights as follows:
          val score = spamminess(features)
          val prob = 1.0 / (1 + exp(-score))
          features.foreach(f => {
            if (w.contains(f)) {
              w(f) += (isSpam - prob) * delta
            } else {
              w(f) = (isSpam - prob) * delta
             }
          })
        })
        w
      })
    trained.saveAsTextFile(args.model())
  }
}
