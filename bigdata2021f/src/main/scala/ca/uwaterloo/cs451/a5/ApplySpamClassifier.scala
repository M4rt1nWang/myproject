package ca.uwaterloo.cs451.a5

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.rogach.scallop._
import scala.collection.mutable.Map

class ApplySpamClassifierConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, model)
  val input = opt[String](descr = "the input test instances", required = true)
  val output = opt[String](descr = "the output directory", required = true)
  val model = opt[String](descr = "the classifier model", required = true)
  verify()
}

object ApplySpamClassifier{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new ApplySpamClassifierConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Model: " + args.model())

    val conf = new SparkConf().setAppName("Apply Spam Classifier")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    val model = sc.textFile(args.model())

    val weight = model.map(line => {
        val data = line.substring(1, line.length()-1).split(",")
        (data.head.toInt, data.last.toDouble)
      }).collectAsMap()

    val weight_map = sc.broadcast(weight)

    def spamminess(features: Array[Int]) : Double = {
      var score = 0d
      features.foreach(f => if (weight_map.value.contains(f)) score += weight_map.value(f))
      score
    }

    val textFile = sc.textFile(args.input())

    val classifier = textFile.map(line => {
      val tokens = line.split("\\s+")
      val docid = tokens.head
      var isSpam = tokens(1)
      val features = tokens.drop(2).map(_.toInt)
      val score = spamminess(features)
      if (score > 0) {
        (docid, isSpam, score, "spam")
      } else {
        (docid, isSpam, score, "ham")
      }
    }).saveAsTextFile(args.output())
  }
}
