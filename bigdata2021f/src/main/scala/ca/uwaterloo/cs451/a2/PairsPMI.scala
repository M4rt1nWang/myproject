package ca.uwaterloo.cs451.a2

import io.bespin.scala.util.Tokenizer

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.rogach.scallop._
import scala.math.log10
import scala.math.min

class PPMIConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, reducers)
  val input = opt[String](descr = "input path", required = true)
  val output = opt[String](descr = "output path", required = true)
  val reducers = opt[Int](descr = "number of reducers", required = false, default = Some(1))
  val threshold = opt[Int](descr = "threshold of co-occurrence", required = false, default = Some(1) )
  verify()
}

object PairsPMI extends Tokenizer {
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new PPMIConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Number of reducers: " + args.reducers())
    log.info("Threshold of co-occurrence: " + args.threshold())

    val conf = new SparkConf().setAppName("Pairs PMI")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    val textFile = sc.textFile(args.input())
    val threshold = args.threshold()
    val total_line: Double = textFile.count()

    val word_map = textFile
      .flatMap(line => {
        val tokens = tokenize(line)
        tokens.take(40).distinct
      })
      .map(word => (word, 1))
      .reduceByKey(_ + _)
      .collectAsMap()

    val broadcastVar = sc.broadcast(word_map)

    val pmi = textFile
      .flatMap(line => {
        val tokens = tokenize(line)
        var result = List[String]()
        if (tokens.length > 1) {
          var len = min(40, tokens.length) 
          val word_set = tokens.take(len).distinct
          for (x <- word_set) {
            for (y <- word_set) {
              if (x == y) {
              } else {
                var s =  x + "," + y
                result =  s :: result
              }
            }
          }
        }
        result
      })
      .map(pair => (pair, 1))
      .reduceByKey(_ + _)
      .filter(pair => pair._2 >= threshold)
      .map(pair => {
        val first = pair._1.split(",")(0)
        val second = pair._1.split(",")(1)
        val pxy = pair._2 / total_line
        val px = broadcastVar.value.getOrElse(first, 0) / total_line
        val py = broadcastVar.value.getOrElse(second, 0) / total_line
        val result_string = "(" + first + "," + second + ")"
        (result_string, (log10(pxy / (px * py)), pair._2))
      })
      .repartition(args.reducers())
    pmi.saveAsTextFile(args.output())
  }
}
