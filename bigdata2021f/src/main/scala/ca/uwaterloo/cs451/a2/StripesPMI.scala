package ca.uwaterloo.cs451.a2

import io.bespin.scala.util.Tokenizer

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.rogach.scallop._
import scala.collection.mutable.Map
import scala.math.min
import scala.math.log10

class SPMIConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, reducers)
  val input = opt[String](descr = "input path", required = true)
  val output = opt[String](descr = "output path", required = true)
  val reducers = opt[Int](descr = "number of reducers", required = false, default = Some(1))
  val threshold = opt[Int](descr = "threshold of co-occurrence", required = false, default = Some(1) )
  verify()
}

object StripesPMI extends Tokenizer {
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new SPMIConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Number of reducers: " + args.reducers())
    log.info("Threshold of co-occurrence: " + args.threshold())

    val conf = new SparkConf().setAppName("Stripes PMI")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    val textFile = sc.textFile(args.input())
    val threshold = args.threshold()
    val total_line: Double = textFile.count()

    val word_map = textFile
      .flatMap(line => {
        val tokens = tokenize(line)
        tokens.take(40).distinct
      })
      .map(word => (word, 1))
      .reduceByKey(_ + _)
      .collectAsMap()

    val broadcastVar = sc.broadcast(word_map)
    
    var result_map = Map[String, Map[String, Int]]()
    val pmi = textFile
      .flatMap(line => {
        result_map.clear()
        val tokens = tokenize(line)
        if (tokens.length > 1) {
            val word_set = tokens.take(min(40, tokens.length)).distinct
            for (x <- word_set) {
              var temp_map = Map[String, Int]()
              for (y <- word_set) {
                if (x == y) {
                } else {
                  temp_map += (y -> 1)
                }
              }
              result_map += (x -> temp_map)
            }
        }
        result_map
      })
      .reduceByKey((map1,map2) => {
        for ((k, v) <- map2) {
          map1 += (k -> (map1.getOrElse(k, 0) + v))
        }
        map1
      })
      .map(pair => {
        var result = Map[String, (Double, Int)]()
        for ((k, v) <- pair._2) {
          if (v > threshold) {
            val pxy = v / total_line
            val px = broadcastVar.value.getOrElse(pair._1, 1) / total_line
            val py = broadcastVar.value.getOrElse(k, 1) / total_line
            val value = (log10(pxy / (px * py)), v)
            result += (k -> value)
          }
        }
        (pair._1, result)
      })
      .filter(pair => !pair._2.isEmpty)
      .repartition(args.reducers())
      .saveAsTextFile(args.output())
  }
}
