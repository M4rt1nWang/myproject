package ca.uwaterloo.cs451.a2

import io.bespin.scala.util.Tokenizer

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.Partitioner
import org.rogach.scallop._

class CBRFPConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, reducers)
  val input = opt[String](descr = "input path", required = true)
  val output = opt[String](descr = "output path", required = true)
  val reducers = opt[Int](descr = "number of reducers", required = false, default = Some(1))
  verify()
}

object ComputeBigramRelativeFrequencyPairs extends Tokenizer {
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new CBRFPConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Number of reducers: " + args.reducers())

    val conf = new SparkConf().setAppName("Compute Bigram Relative Frequency Pairs")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    var sum = 0.0f
    val textFile = sc.textFile(args.input())
    val counts = textFile
      .flatMap(line => {
        val tokens = tokenize(line)
        if (tokens.length > 1) {
          tokens.sliding(2).map(p => (p.head, p.last)).toList ++ tokens.dropRight(1).map(p => (p, "*")).toList
        } else {
          List()
        }
      })
      .map(bigram => (bigram, 1.0f))
      .reduceByKey(_ + _)
      .groupBy((pair:((String, String), Float)) => pair._1._1, args.reducers())
      .flatMap(pair => {
        pair._2.toList.sortBy(_._1)
      })
      .map(pair => {
        val key = pair._1
        val entry = pair._2
        if (key._2 == "*") {
          sum = entry
          (key, entry)
        } else {
          (key, entry / sum)
        }
      })
      .saveAsTextFile(args.output())
  }
}
