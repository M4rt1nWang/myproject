package ca.uwaterloo.cs451.a2

import io.bespin.scala.util.Tokenizer

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.Partitioner
import scala.collection.mutable.Map
import scala.collection.mutable.ListBuffer
import org.rogach.scallop._

class CBRFSConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, output, reducers)
  val input = opt[String](descr = "input path", required = true)
  val output = opt[String](descr = "output path", required = true)
  val reducers = opt[Int](descr = "number of reducers", required = false, default = Some(1))
  verify()
}

object ComputeBigramRelativeFrequencyStripes extends Tokenizer {
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new CBRFSConf(argv)

    log.info("Input: " + args.input())
    log.info("Output: " + args.output())
    log.info("Number of reducers: " + args.reducers())

    val conf = new SparkConf().setAppName("Compute Bigram Relative Frequency Stripes")
    val sc = new SparkContext(conf)

    val outputDir = new Path(args.output())
    FileSystem.get(sc.hadoopConfiguration).delete(outputDir, true)

    var stripes = ListBuffer[(String, Map[String, Float])]()
    var sum = 0.0f
    val textFile = sc.textFile(args.input())
    val counts = textFile
      .flatMap(line => {
        stripes.clear()
        val tokens = tokenize(line)
        if (tokens.length > 1) {
          for (i <- 1 to tokens.length - 1) {
            val prev = tokens(i - 1)
            val curr = tokens(i)
            stripes += ((prev, Map(curr -> 1.0f)))
          }
        }
        stripes
      })
      .reduceByKey((map1, map2) => {
        for ((k, v) <- map2) {
          map1 += (k -> (map1.getOrElse(k, 0.0f) + v))
        }
        map1
      })
      .map(stripe => {
        var result_map = Map[String, Float]()
        sum = 0.0f
        for ((k, v) <- stripe._2) {
          sum += v
        }
        for((k, v) <- stripe._2) {
          result_map += (k -> (v / sum))
        }
        (stripe._1, result_map)
      })
      .repartition(args.reducers())
      .saveAsTextFile(args.output())
  }
}
