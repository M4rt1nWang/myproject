/**
  * Bespin: reference implementations of "big data" algorithms
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

package ca.uwaterloo.cs451.a7 

import java.io.File
import java.util.concurrent.atomic.AtomicInteger

import org.apache.log4j._
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{ManualClockWrapper, Minutes, StreamingContext, State, Time, StateSpec}
import org.apache.spark.streaming.scheduler.{StreamingListener, StreamingListenerBatchCompleted}
import org.apache.spark.util.LongAccumulator
import org.rogach.scallop._

import scala.collection.mutable

class TrendingArrivalsConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, checkpoint, output)
  val input = opt[String](descr = "input path", required = true)
  val checkpoint = opt[String](descr = "checkpoint path", required = true)
  val output = opt[String](descr = "output path", required = true)
  verify()
}

object TrendingArrivals {
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]): Unit = {
    val args = new TrendingArrivalsConf(argv)

    log.info("Input: " + args.input())

    val spark = SparkSession
      .builder()
      .config("spark.streaming.clock", "org.apache.spark.util.ManualClock")
      .appName("TrendingArrivals")
      .getOrCreate()

    val numCompletedRDDs = spark.sparkContext.longAccumulator("number of completed RDDs")

    val batchDuration = Minutes(1)
    val ssc = new StreamingContext(spark.sparkContext, batchDuration)
    val batchListener = new StreamingContextBatchCompletionListener(ssc, 144)
    ssc.addStreamingListener(batchListener)

    val rdds = buildMockStream(ssc.sparkContext, args.input())
    val inputData: mutable.Queue[RDD[String]] = mutable.Queue()
    val stream = ssc.queueStream(inputData)

    def trackStateFunc(batch: Time, key: String, value: Option[Int], state: State[(Int, Long, Int)]) : Option[(String, (Int, Long, Int))] = {
      val current = value.getOrElse(0)
      val past = state.getOption.getOrElse((0, 0, 0))._1
      val cur_time = batch.milliseconds
      if ((current >= (2 * past)) && (current >= 10)) {
        if (key == "goldman") {
          println("Number of arrivals to Goldman Sachs has doubled from " + past.toString + " to " + current.toString + " at "+ cur_time + "!")
        } else if (key == "citigroup") {
          println("Number of arrivals to Citigroup has doubled from " + past.toString + " to " + current.toString + " at "+ cur_time + "!")
        }
      }
      val new_value = (current, cur_time, past)
      state.update(new_value)
      val output = (key, new_value)
      Some(output)
    }

    val wc = stream.map(_.split(","))
      .map(list => {
        var longitude = 0.0d
        var latitude = 0.0d
        if (list(0) == "yellow") {
          longitude = list(10).toDouble
          latitude = list(11).toDouble
        } else {
          longitude = list(8).toDouble
          latitude = list(9).toDouble
        }
        if ((-74.0144185 < longitude) && (longitude < -74.013777) && (40.7138745 < latitude) && (latitude < 40.7152275)) {
          ("goldman", 1)
        } else if ((-74.012083 < longitude) && (longitude < -74.009867) && (40.720053 < latitude) && (latitude < 40.7217236)) {
          ("citigroup", 1)
        } else {
          ("other", 1)
        }
      })
      .filter(_._1 != "other")
      .reduceByKeyAndWindow(
        (x: Int, y: Int) => x + y, (x: Int, y: Int) => x - y, Minutes(10), Minutes(10))
      .mapWithState(StateSpec.function(trackStateFunc _))

    val output = args.output()
    wc.stateSnapshots().foreachRDD((rdd, time) => {
      rdd.saveAsTextFile(output + "/part-" + "%08d".format(time.milliseconds))
      numCompletedRDDs.add(1L)
    })
    ssc.checkpoint(args.checkpoint())
    ssc.start()

    for (rdd <- rdds) {
      inputData += rdd
      ManualClockWrapper.advanceManualClock(ssc, batchDuration.milliseconds, 50L)
    }

    batchListener.waitUntilCompleted(() =>
      ssc.stop()
    )
  }

  class StreamingContextBatchCompletionListener(val ssc: StreamingContext, val limit: Int) extends StreamingListener {
    def waitUntilCompleted(cleanUpFunc: () => Unit): Unit = {
      while (!sparkExSeen) {}
      cleanUpFunc()
    }

    val numBatchesExecuted = new AtomicInteger(0)
    @volatile var sparkExSeen = false

    override def onBatchCompleted(batchCompleted: StreamingListenerBatchCompleted) {
      val curNumBatches = numBatchesExecuted.incrementAndGet()
      log.info(s"${curNumBatches} batches have been executed")
      if (curNumBatches == limit) {
        sparkExSeen = true
      }
    }
  }

  def buildMockStream(sc: SparkContext, directoryName: String): Array[RDD[String]] = {
    val d = new File(directoryName)
    if (d.exists() && d.isDirectory) {
      d.listFiles
        .filter(file => file.isFile && file.getName.startsWith("part-"))
        .map(file => d.getAbsolutePath + "/" + file.getName).sorted
        .map(path => sc.textFile(path))
    } else {
      throw new IllegalArgumentException(s"$directoryName is not a valid directory containing part files!")
    }
  }
}
