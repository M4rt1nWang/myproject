package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class Q5Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q5{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q5Conf(argv)

    log.info("Input: " + args.input())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q5")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
   
    if (args.text()) {
      val customer = sc.textFile(args.input()+"/customer.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(3).toInt))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val nation = sc.textFile(args.input()+"/nation.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1)))
        .collectAsMap()

      val nation_map = sc.broadcast(nation)
        
      val orders = sc.textFile(args.input() + "/orders.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1).toInt))

      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(10).toString.substring(0, 7)))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[((Int, String, String), Int)]()
          val target = line._2._1.toList
          for (dt <- target) {
            val nationkey = customer_map.value(line._2._2.toList(0))
            val temp = ((nationkey, nation_map.value(nationkey), dt), 1)
            result += temp
          }
          result.toList
        })
        .filter(pair => (pair._1._1.toInt == 3) || (pair._1._1.toInt == 24))
        .reduceByKey(_+_)
        .sortByKey()
        .collect()
        .map(pair => (pair._1._1, pair._1._2, pair._1._3, pair._2))
        .foreach(println)
        //.saveAsTextFile("temp")
    } else {
      val customer = sparkSession.read.parquet(args.input()+"/customer").rdd
        .map(line => (line(0).toString.toInt, line(3).toString.toInt))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val nation = sparkSession.read.parquet(args.input()+"/nation").rdd
        .map(line => (line(0).toString.toInt, line(1).toString))
        .collectAsMap()

      val nation_map = sc.broadcast(nation)

      val orders = sparkSession.read.parquet(args.input() + "/orders").rdd
        .map(line => (line(0).toString.toInt, line(1).toString.toInt))

      val lineitem = sparkSession.read.parquet(args.input() + "/lineitem").rdd
        .map(line => (line(0).toString.toInt, line(10).toString.substring(0, 7)))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[((Int, String, String), Int)]()
          val target = line._2._1.toList
          for (dt <- target) {
            val nationkey = customer_map.value(line._2._2.toList(0))
            val temp = ((nationkey, nation_map.value(nationkey), dt), 1)
            result += temp
          }
          result.toList
        })
        .filter(pair => (pair._1._1.toInt == 3) || (pair._1._1.toInt == 24))
        .reduceByKey(_+_)
        .sortByKey()
        .collect()
        .map(pair => (pair._1._1, pair._1._2, pair._1._3, pair._2))
        .foreach(println)
    }
  }
}
