package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._

class Q6Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q6{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q6Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q6")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate

    val date = args.date()

    if (args.text()) {
      var textFile = sc.textFile(args.input() + "/lineitem.tbl")
      val count = textFile
        .filter(row => row.split("\\|")(10) == date)
        .map(row => {
          val splited = row.split("\\|")
          val key = (splited(8), splited(9))
          val discount = splited(6).toDouble
          val tax = splited(7).toDouble
          val value = (splited(4).toDouble,
                       splited(5).toDouble,
                       splited(5).toDouble * (1 - discount),
                       splited(5).toDouble * (1 - discount) * (1 + tax),
                       discount,
                       1)
          (key, value)
        })
        .reduceByKey((key1, key2) => {
          (key1._1 + key2._1,
           key1._2 + key2._2,
           key1._3 + key2._3,
           key1._4 + key2._4, 
           key1._5 + key2._5, 
           key1._6 + key2._6)
        })
        .sortByKey()
        .collect()
        .map(pair => {
          val total = pair._2._6
          (pair._1._1,
           pair._1._2, 
           pair._2._1, 
           pair._2._2, 
           pair._2._3,
           pair._2._4, 
           pair._2._1 / total, 
           pair._2._2 / total, 
           pair._2._5 / total, 
           total.toInt)
        })
        .foreach(println)
    } else {
      var textFile = sparkSession.read.parquet(args.input() + "/lineitem").rdd
      val count = textFile
        .filter(row => row(10).toString == date)
        .map(row => {
          val key = (row(8).toString, row(9).toString)
          val discount = row(6).toString.toDouble
          val tax = row(7).toString.toDouble
          val value = (row(4).toString.toDouble,
                       row(5).toString.toDouble,
                       row(5).toString.toDouble * (1 - discount),
                       row(5).toString.toDouble * (1 - discount) * (1 + tax),
                       discount,
                       1)
          (key, value)
        })
        .reduceByKey((key1, key2) => {
          (key1._1 + key2._1,
           key1._2 + key2._2,
           key1._3 + key2._3,
           key1._4 + key2._4, 
           key1._5 + key2._5, 
           key1._6 + key2._6)
        })
        .sortByKey()
        .collect()
        .map(pair => {
          val total = pair._2._6
          (pair._1._1,
           pair._1._2, 
           pair._2._1, 
           pair._2._2, 
           pair._2._3,
           pair._2._4, 
           pair._2._1 / total, 
           pair._2._2 / total, 
           pair._2._5 / total, 
           total.toInt)
        })
        .foreach(println)
    }
  }
}
