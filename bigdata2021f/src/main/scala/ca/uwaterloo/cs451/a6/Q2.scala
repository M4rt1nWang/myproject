package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class Q2Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q2{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q2Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q2")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
    
    val date = args.date()

    if (args.text()) {
      val orders = sc.textFile(args.input() + "/orders.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(6)))
      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .filter(line => line.split("\\|")(10) == date)
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(10)))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[(String, Int)]()
          val target = line._2._1.toList
          for (dt <- target) {
            val temp = (line._2._2.toList(0), line._1)
            result += temp
          }
          result.toList
        })
        .sortBy(_._2)
        .take(20)
        .foreach(println)
    } else {
      val orders = sparkSession.read.parquet(args.input() + "/orders").rdd
        .map(line => (line(0).toString.toInt, line(6)))
      val lineitem = sparkSession.read.parquet(args.input() + "/lineitem").rdd
        .filter(line => line(10).toString == date)
        .map(line => (line(0).toString.toInt, line(10)))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[(String, Int)]()
          val target = line._2._1.toList
          for (dt <- target) {
            val temp = (line._2._2.toList(0).toString, line._1)
            result += temp
          }
          result.toList
        })
        .sortBy(_._2)
        .take(20)
        .foreach(println)
    }
  }
}
