package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class Q4Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q4{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q4Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q4")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
    
    val date = args.date()

    if (args.text()) {
      val customer = sc.textFile(args.input()+"/customer.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(3).toInt))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val nation = sc.textFile(args.input()+"/nation.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1)))
        .collectAsMap()

      val nation_map = sc.broadcast(nation)
        
      val orders = sc.textFile(args.input() + "/orders.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1).toInt))

      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .filter(line => line.split("\\|")(10) == date)
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(10)))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[((Int, String), Int)]()
          val target = line._2._1.toList
          val nation_key = customer_map.value(line._2._2.toList(0))
          for (dt <- target) {
            val temp = ((nation_key, nation_map.value(nation_key)), 1)
            result += temp
          }
          result.toList
        })
        .reduceByKey(_+_)
        .map(pair => (pair._1._1, pair._1._2, pair._2))
        .sortBy(pair => pair._1)
        .collect()
        .foreach(println)
    } else {
      val customer = sparkSession.read.parquet(args.input()+"/customer").rdd
        .map(line => (line(0).toString.toInt, line(3).toString.toInt))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val nation = sparkSession.read.parquet(args.input()+"/nation").rdd
        .map(line => (line(0).toString.toInt, line(1).toString))
        .collectAsMap()

      val nation_map = sc.broadcast(nation)

      val orders = sparkSession.read.parquet(args.input() + "/orders").rdd
        .map(line => (line(0).toString.toInt, line(1).toString.toInt))

      val lineitem = sparkSession.read.parquet(args.input() + "/lineitem").rdd
        .filter(line => line(10).toString == date)
        .map(line => (line(0).toString.toInt, line(10).toString))
        .cogroup(orders)
        .flatMap(line => {
          val result = ListBuffer[((Int, String), Int)]()
          val target = line._2._1.toList
          val nation_key = customer_map.value(line._2._2.toList(0))
          for (dt <- target) {
            val temp = ((nation_key, nation_map.value(nation_key)), 1)
            result += temp
          }
          result.toList
        })
        .reduceByKey(_+_)
        .map(pair => (pair._1._1, pair._1._2, pair._2))
        .sortBy(pair => pair._1)
        .collect()
        .foreach(println)
    }
  }
}
