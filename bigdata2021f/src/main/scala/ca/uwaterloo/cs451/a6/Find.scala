package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class FindConf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Find{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new FindConf(argv)

    log.info("Input: " + args.input())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Find")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
    
    if (args.text()) {
      val orders = sc.textFile(args.input() + "/orders.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(6)))
      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(10)))
        .cogroup(orders)
        .filter(line => {
          var result = false
          val temp = line._2._1.toList
          for (a <- 0 to temp.size - 1) {
            val dt = temp(a)
            for (b <- (a+1) to temp.size - 1) {
              if (temp(b) == dt) {
                result = true
              }
            }
          }
          result
        })
        .foreach(println)
    } else {
    }
  }
}

