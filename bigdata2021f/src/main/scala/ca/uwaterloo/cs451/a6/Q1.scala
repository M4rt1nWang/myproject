package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._

class Q1Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q1{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q1Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q1")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate

    val date = args.date()

    if (args.text()) {
      var textFile = sc.textFile(args.input() + "/lineitem.tbl")
      val count = textFile
        .filter(row => row.split("\\|")(10) == date)
        .count
      println("ANSWER=" + count)
    } else {
      var textFile = sparkSession.read.parquet(args.input() + "/lineitem").rdd
      val count = textFile
        .filter(row => row(10).toString == date)
        .count
      println("ANSWER=" + count)
    }
  }
}
