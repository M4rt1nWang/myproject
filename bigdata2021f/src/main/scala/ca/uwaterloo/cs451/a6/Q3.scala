package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class Q3Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q3{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q3Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q3")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
    
    val date = args.date()

    if (args.text()) {
      val part = sc.textFile(args.input()+"/part.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1)))
        .collectAsMap()

      val part_map = sc.broadcast(part)

      val supplier = sc.textFile(args.input()+"/supplier.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1)))
        .collectAsMap()

      val supplier_map = sc.broadcast(supplier)

      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .filter(line => line.split("\\|")(10) == date)
        .map(line => {
          val l = line.split("\\|")
          (l(0).toInt, l(1).toInt, l(2).toInt, l(10))
        })
        .map(tuple => (tuple._1, part_map.value(tuple._2), supplier_map.value(tuple._3)))
        .sortBy(tuple => tuple._1)
        .take(20)
        .foreach(println)
    } else {
      val part = sparkSession.read.parquet(args.input() + "/part").rdd
        .map(line => (line(0).toString.toInt, line(1).toString))
        .collectAsMap()

      val part_map = sc.broadcast(part)

      val supplier = sparkSession.read.parquet(args.input() + "/supplier").rdd
        .map(line => (line(0).toString.toInt, line(1).toString))
        .collectAsMap()

      val supplier_map = sc.broadcast(supplier)

      val lineitem = sparkSession.read.parquet(args.input() + "/lineitem").rdd
        .filter(line => line(10).toString == date)
        .map(line => (line(0).toString.toInt, line(1).toString.toInt, line(2).toString.toInt, line(10).toString))
        .map(tuple => (tuple._1, part_map.value(tuple._2), supplier_map.value(tuple._3)))
        .sortBy(tuple => tuple._1)
        .take(20)
        .foreach(println)
    }
  }
}
