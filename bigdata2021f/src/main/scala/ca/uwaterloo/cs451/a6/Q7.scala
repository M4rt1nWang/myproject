package ca.uwaterloo.cs451.a6

import org.apache.log4j._
import org.apache.hadoop.fs._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.rogach.scallop._
import scala.collection.mutable.ListBuffer

class Q7Conf(args: Seq[String]) extends ScallopConf(args) {
  mainOptions = Seq(input, date)
  val input = opt[String](descr = "the directory that contains the data", required = true)
  val date = opt[String](descr = "argument corresponds to the l_shipdate predicate in the SQL query", required = true)
  val text = opt[Boolean](descr = "data is in text")
  val parquet = opt[Boolean](descr = "data is in parquet")
  verify()
}

object Q7{
  val log = Logger.getLogger(getClass().getName())

  def main(argv: Array[String]) {
    val args = new Q7Conf(argv)

    log.info("Input: " + args.input())
    log.info("Date: " + args.date())
    if (args.text()){
      log.info("Text: True")
    } else {
      log.info("Parquet:True")
    }

    val conf = new SparkConf().setAppName("Q7")
    val sc = new SparkContext(conf)
    val sparkSession = SparkSession.builder.getOrCreate
    
    val date = args.date()

    if (args.text()) {
      val customer = sc.textFile(args.input()+"/customer.tbl")
        .map(line => (line.split("\\|")(0).toInt, line.split("\\|")(1)))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val orders = sc.textFile(args.input() + "/orders.tbl")
        .filter(line => line.split("\\|")(4) < date)
        .map(line => {
          val splited = line.split("\\|")
          (splited(0).toInt, (splited(1).toInt, splited(4), splited(7).toInt))
        })

      val lineitem = sc.textFile(args.input() + "/lineitem.tbl")
        .filter(line => line.split("\\|")(10) > date)
        .map(line =>{
          val splited = line.split("\\|")
          (splited(0).toInt, (splited(5).toDouble * (1 - splited(6).toDouble)))
        })
        .cogroup(orders)
        .filter(pair => pair._2._1.size != 0 && pair._2._2.size != 0)
        .map(line => {
          var sum = 0.0d
          val target = line._2._1.toList
          for (dt <- target) {
            sum += dt.toDouble
          }
          ((customer_map.value(line._2._2.toList(0)._1), line._1, line._2._2.toList(0)._2, line._2._2.toList(0)._3), sum)
        })
        .reduceByKey(_ + _)
        .sortBy(_._2, false)
        .collect()
        .take(10)
        .foreach(pair => println((pair._1._1, pair._1._2, pair._2, pair._1._3, pair._1._4)))
    } else {
      val customer = sparkSession.read.parquet(args.input() + "/customer").rdd
        .map(line => (line(0).toString.toInt, line(1).toString))
        .collectAsMap()

      val customer_map = sc.broadcast(customer)

      val orders = sparkSession.read.parquet(args.input() + "/orders").rdd
        .filter(line => line(4).toString < date)
        .map(line => (line(0).toString.toInt, (line(1).toString.toInt, line(4).toString, line(7).toString.toInt)))

      val lineitem = sparkSession.read.parquet(args.input() + "/lineitem").rdd
        .filter(line => line(10).toString > date)
        .map(line => (line(0).toString.toInt, (line(5).toString.toDouble * (1 - line(6).toString.toDouble))))
        .cogroup(orders)
        .filter(pair => pair._2._1.size != 0 && pair._2._2.size != 0)
        .map(line => {
          var sum = 0.0d
          val target = line._2._1.toList
          for (dt <- target) {
            sum += dt.toDouble
          }
          ((customer_map.value(line._2._2.toList(0)._1), line._1, line._2._2.toList(0)._2, line._2._2.toList(0)._3), sum)
        })
        .reduceByKey(_ + _)
        .sortBy(_._2, false)
        .collect()
        .take(10)
        .foreach(pair => println((pair._1._1, pair._1._2, pair._2, pair._1._3, pair._1._4)))
    }
  }
}

