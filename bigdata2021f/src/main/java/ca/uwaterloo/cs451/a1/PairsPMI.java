package ca.uwaterloo.cs451.a1;
import io.bespin.java.util.Tokenizer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.ParserProperties;
import tl.lin.data.pair.PairOfStrings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.*;

public class PairsPMI extends Configured implements Tool {
  private static final Logger LOG = Logger.getLogger(PairsPMI.class);
  private static final int MAX_WORD = 40;

  private static final class MyMapper extends Mapper<LongWritable, Text, Text, IntWritable> {
    private static final Text WORD = new Text();
    private static final IntWritable ONE = new IntWritable(1);
    public static enum CountLines { LINENUM };

    @Override
    public void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {
      Counter counter = context.getCounter(CountLines.LINENUM);
      counter.increment(1);
      Set<String> word_set = new HashSet<String>();
      int word_checked = 0;
      for (String word : Tokenizer.tokenize(value.toString())) {
        if (word_set.add(word)) {
          WORD.set(word);
          context.write(WORD, ONE);
        }
        word_checked++;
        if (word_checked >= MAX_WORD) {
          break;
        }
      }
    }
  }

  private static final class MyReducer extends
      Reducer<Text, IntWritable, Text, IntWritable> {
    private static final IntWritable SUM = new IntWritable();

    @Override
    public void reduce(Text key, Iterable<IntWritable> values, Context context)
        throws IOException, InterruptedException {
      Iterator<IntWritable> iter = values.iterator();
      int sum = 0;
      while (iter.hasNext()) {
        sum += iter.next().get();
      }

      SUM.set(sum);
      context.write(key, SUM);
    }
  }


  private static final class MyMapper2 extends Mapper<LongWritable, Text, PairOfStrings, IntWritable> {

    private static final PairOfStrings PAIR = new PairOfStrings();
    private static final IntWritable ONE = new IntWritable(1);

    @Override
    public void map(LongWritable key, Text value, Context context)
        throws IOException, InterruptedException {

      Set<String> word_set = new HashSet<String>();
      int word_checked = 0;
      for (String word : Tokenizer.tokenize(value.toString())) {
        word_set.add(word);
        word_checked++;
        if (word_checked >= MAX_WORD) {
          break;
        }
      }
      for (String a : word_set) {
        for (String b : word_set) {
          if (a == b) continue;
          PAIR.set(a, b);
          context.write(PAIR, ONE);
        }
      }
    }
  }

  public static final class MyCombiner2 extends
      Reducer<PairOfStrings, IntWritable, PairOfStrings, IntWritable> {
    private static final IntWritable SUM = new IntWritable();

    @Override
    public void reduce(PairOfStrings key, Iterable<IntWritable> values, Context context)
        throws IOException, InterruptedException {
      Iterator<IntWritable> iter = values.iterator();
      int sum = 0;
      while (iter.hasNext()) {
        sum += iter.next().get();
      }
      SUM.set(sum);
      context.write(key, SUM);
    }
  }

  public static final class MyReducer2 extends Reducer<PairOfStrings, IntWritable, PairOfStrings, PairOfStrings> {
    private static final PairOfStrings PMI = new PairOfStrings();
    private static final Map<String, Integer> word_occurence = new HashMap<String, Integer>();

    private static long linenum;
    private static int threshold;

    @Override
    public void setup(Context context) throws IOException, InterruptedException {
      Configuration config = context.getConfiguration();
      threshold = config.getInt("threshold", 10);
      linenum = config.getLong("counter", 0);

      FileSystem filesystem = FileSystem.get(config);
      Path path = new Path("temp/");
      FileStatus[] filestatuses  = filesystem.listStatus(path);
      for (FileStatus filestatus : filestatuses) {
        String filename = filestatus.getPath().toString();
        if (filename.matches("(.*)temp/part(.*)")) {
          FSDataInputStream filestream = filesystem.open(filestatus.getPath());
          InputStreamReader inputstream = new InputStreamReader(filestream);
          BufferedReader buffer = new BufferedReader(inputstream);
          String line = buffer.readLine();
          while (line != null) {
            String[] pair = line.split("\\s+");
            if(pair.length == 2){
              word_occurence.put(pair[0], Integer.parseInt(pair[1]));
            }
            line = buffer.readLine();
          }
          buffer.close();
        }
      }
    }

    @Override
    public void reduce(PairOfStrings key, Iterable<IntWritable> values, Context context)
        throws IOException, InterruptedException {
      Iterator<IntWritable> iter = values.iterator();
      int sum = 0;
      while (iter.hasNext()) {
        sum += iter.next().get();
      }

      if (sum >= threshold) {
        double linenumDouble = (double) linenum;
        double pxy = (double)sum / linenum;
        double px = (double)word_occurence.get(key.getLeftElement()) / linenum;
        double py = (double)word_occurence.get(key.getRightElement()) / linenum;
        float pmi = (float)Math.log10(pxy / (px * py));
        PMI.set(Float.toString(pmi), Integer.toString(sum));
        context.write(key, PMI);
      }
    }
  }


  /**
   * Creates an instance of this tool.
   */
  private PairsPMI() {}

  private static final class Args {
    @Option(name = "-input", metaVar = "[path]", required = true, usage = "input path")
    String input;

    @Option(name = "-output", metaVar = "[path]", required = true, usage = "output path")
    String output;

    @Option(name = "-reducers", metaVar = "[num]", usage = "number of reducers")
    int numReducers = 1;

    @Option(name = "-threshold", metaVar = "[num]", usage = "the threshold of co-occurrence")
    int threshold = 10;
  }

  /**
   * Runs this tool.
   */
  @Override
  public int run(String[] argv) throws Exception {
    final Args args = new Args();
    CmdLineParser parser = new CmdLineParser(args, ParserProperties.defaults().withUsageWidth(100));

    try {
      parser.parseArgument(argv);
    } catch (CmdLineException e) {
      System.err.println(e.getMessage());
      parser.printUsage(System.err);
      return -1;
    }

    LOG.info("Tool: " + PairsPMI.class.getSimpleName());
    LOG.info(" - input path: " + args.input);
    LOG.info(" - output path: " + args.output);
    LOG.info(" - number of reducers: " + args.numReducers);
    LOG.info(" - threshold: " + args.threshold);

    Configuration conf = getConf();

    Job job1 = Job.getInstance(conf);
    job1.getConfiguration().setInt("mapred.max.split.size", 1024 * 1024 * 32);
    job1.getConfiguration().set("mapreduce.map.memory.mb", "3072");
    job1.getConfiguration().set("mapreduce.map.java.opts", "-Xmx3072m");
    job1.getConfiguration().set("mapreduce.reduce.memory.mb", "3072");
    job1.getConfiguration().set("mapreduce.reduce.java.opts", "-Xmx3072m");
    job1.setJobName(PairsPMI.class.getSimpleName());
    job1.setJarByClass(PairsPMI.class);

    // Delete the output directory if it exists already.
    Path outputDir = new Path("temp/");
    FileSystem.get(conf).delete(outputDir, true);

    job1.setNumReduceTasks(args.numReducers);

    FileInputFormat.setInputPaths(job1, new Path(args.input));
    FileOutputFormat.setOutputPath(job1, new Path("temp/"));

    job1.setMapOutputKeyClass(Text.class);
    job1.setMapOutputValueClass(IntWritable.class);
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(IntWritable.class);

    job1.setMapperClass(MyMapper.class);
    job1.setCombinerClass(MyReducer.class);
    job1.setReducerClass(MyReducer.class);
    job1.setOutputFormatClass(TextOutputFormat.class);

    long startTime = System.currentTimeMillis();
    job1.waitForCompletion(true);
    System.out.println("Job Finished in " + (System.currentTimeMillis() - startTime) / 1000.0 + " seconds");

    // Job 2 begins
    long count = job1.getCounters().findCounter(MyMapper.CountLines.LINENUM).getValue();
    conf.setLong("counter", count);
    conf.setInt("threshold", args.threshold);
    Job job2 = Job.getInstance(conf);
    job2.getConfiguration().setInt("mapred.max.split.size", 1024 * 1024 * 32);
    job2.getConfiguration().set("mapreduce.map.memory.mb", "3072");
    job2.getConfiguration().set("mapreduce.map.java.opts", "-Xmx3072m");
    job2.getConfiguration().set("mapreduce.reduce.memory.mb", "3072");
    job2.getConfiguration().set("mapreduce.reduce.java.opts", "-Xmx3072m");
    job2.setJobName(PairsPMI.class.getSimpleName());
    job2.setJarByClass(PairsPMI.class);
    job2.setNumReduceTasks(args.numReducers);

    // Delete the output directory if it exists already.
    outputDir = new Path(args.output);
    FileSystem.get(conf).delete(outputDir, true);

    FileInputFormat.setInputPaths(job2, new Path(args.input));
    FileOutputFormat.setOutputPath(job2, new Path(args.output));

    job2.setMapOutputKeyClass(PairOfStrings.class);
    job2.setMapOutputValueClass(IntWritable.class);
    job2.setOutputKeyClass(PairOfStrings.class);
    job2.setOutputValueClass(PairOfStrings.class);
    job2.setOutputFormatClass(TextOutputFormat.class);

    job2.setMapperClass(MyMapper2.class);
    job2.setCombinerClass(MyCombiner2.class);
    job2.setReducerClass(MyReducer2.class);
    startTime = System.currentTimeMillis();
    job2.waitForCompletion(true);
    LOG.info("Job Finished in " + (System.currentTimeMillis() - startTime) / 1000.0 + " seconds");
    return 0;
  }

  /**
   * Dispatches command-line arguments to the tool via the {@code ToolRunner}.
   *
   * @param args command-line arguments
   * @throws Exception if tool encounters an exception
   */
  public static void main(String[] args) throws Exception {
    ToolRunner.run(new PairsPMI(), args);
  }
}

