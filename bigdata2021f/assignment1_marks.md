# CS 451/651 Big Data Infrastructure (Fall 2021)
## Assignment 1 Marking

**Student details**
Number:20759526
WATIAM:j2228wan
**Test 1**
Linux - Compile and run
mark: 10.0/10.0

**Test 2**
Linux - Number of distinct PMI pairs
mark: 2.0/2.0

**Test 3**
Linux - Lowest PMI values
mark: 2.0/2.0

**Test 4**
Linux - Highest PMI values
mark: 2.0/2.0

**Test 5**
Linux - High PMI with "tears"
mark: 2.0/2.0

**Test 6**
Linux - High PMI with "love"
mark: 2.0/2.0

**Test 7**
Datasci - Compile and run
mark: 10.0/10.0

**Test 8**
Datasci - High PMI with "hockey"
mark: 2.0/2.0

**Test 9**
Datasci - High PMI with "data"
mark: 2.0/2.0

**Test 10**
Datasci - High PMI with "tears"
mark: 1.0/1.0

**Final grade**
mark: 35.0/35.0

summary: 20759526,j2228wan,35.0/35.0