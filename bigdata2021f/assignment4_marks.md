# CS 451/651 Big Data Infrastructure (Fall 2021)
## Assignment 4 Marking

**Student details**
Number:20759526
WATIAM:j2228wan

**Test 1**
single source correctness on linux
mark: 10/10

**Test 2**
multi source correctness on linux
mark: 25/25

**Test 3**
single source correctness on datasci
mark: 5/5

**Test 4**
multi source correctness on datasci
mark: 15/15

**feedback :** 

**Final grade**
mark: 55.0/55.0

summary: 20759526,j2228wan,55.0/55.0