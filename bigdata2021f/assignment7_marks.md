# CS 451/651 Data-Intensive Distributed Computing (Fall 2021)
## Assignment 7 Marking

**Student details**
Number:20759526
WATIAM:j2228wan

**Test 1** Q1 mark: 5.0/5.0

**Test 2** Q2. citigroup mark: 5.0/5.0

**Test 3** Q2. goldman mark: 5.0/5.0

**Test 4** Q3. citigroup mark: 5.0/5.0

**Test 5** Q3. goldman mark: 5.0/5.0

**Test 6** Q3. trend detector mark: 5.0/5.0

**Feedback :** 

**Final grade**
mark: 30.0/30.0

summary: 20759526,j2228wan,30.0/30.0