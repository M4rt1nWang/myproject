# CS 451/651 Big Data Infrastructure (Fall 2021)
## Assignment 2 Marking

**Student details**
Number:20759526
WATIAM:j2228wan
**Test 1**
Bigram Relative Frequency - Pair Impl on Linux
mark: 5.0/5

**Test 2**
Bigram Relative Frequency - Stripe Impl on Linux
mark: 5.0/5

**Test 3**
PMI - Pair Impl on Linux
mark: 5.0/5

**Test 4**
PMI - Stripe Impl on Linux
mark: 4.82/5

**Test 5**
Bigram Relative Frequency - Pair Impl on Datasci
mark: 5.0/5

**Test 6**
Bigram Relative Frequency - Stripe Impl on Datasci
mark: 5.0/5

**Test 7**
PMI - Pair Impl on Datasci
mark: 5.0/5

**Test 8**
PMI - Stripe Impl on Datasci
mark: 4.69/5

**Final grade**
mark: 39.51/40.0

summary: 20759526,j2228wan,39.51/40.0