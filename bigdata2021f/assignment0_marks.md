# CS 451/651 Big Data Infrastructure (Fall 2021)
## Assignment 0 Marking

**Student details**
Number:20759526
WATIAM:j2228wan

**Question 1**
mark: 2/2

**Question 2**
mark: 2/2

**Question 3**
mark: 2/2

**Question 4**
mark: 2/2

**Question 5**
mark: 2/2

**Test 1**
PerfectX on Linux
mark: 5/5

**Test 2**
PerfectX on Datasci
mark: 5/5

**feedback :** Stay safe :)

**Final grade**
mark: 20.0/20.0

summary: 20759526,j2228wan,20.0/20.0